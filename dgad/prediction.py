import json
import logging
import os
from dataclasses import asdict, dataclass, field
from importlib import resources
from pathlib import Path
from typing import Any, Dict, List, Optional, Set, Tuple

import numpy as np
import tensorflow
from tcn import TCN

import dgad.label_encoders
import dgad.models
from dgad.schema import Domain, Word
from dgad.utils import load_labels, separate_domains_that_are_too_long

os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"


def default_binary_labels() -> Dict[int, str]:
    return {0: "benign", 1: "DGA"}


def default_custom_objects() -> Dict[str, str]:
    return {}


@dataclass
class Model:
    filepath: Path
    data: Any = None
    labels: Dict[int, str] = field(default_factory=default_binary_labels)
    optimizer: str = "adam"
    loss: str = "binary_crossentropy"
    custom_objects: Dict[str, str] = field(default_factory=default_custom_objects)

    def __post_init__(self) -> None:
        self.data = tensorflow.keras.models.load_model(
            filepath=self.filepath, custom_objects={"TCN": TCN}
        )
        self.data.compile(loss=self.loss, optimizer=self.optimizer)


class Detective:
    statistical_significance = 0.5

    def __init__(
        self, model_binary: Optional[Model] = None, model_multi: Optional[Model] = None
    ) -> None:
        # use included binary model if one is not provided
        if model_binary:
            self.model_binary = model_binary
        else:
            with resources.path(dgad.models, "tcn_best.h5") as model_path:
                self.model_binary = Model(
                    filepath=model_path, custom_objects={"TCN": TCN}
                )
        # use included family model if one is not provided
        if model_multi:
            self.model_multi = model_multi
        else:
            with resources.path(dgad.models, "tcn_family_81_classes.h5") as model_path:
                with resources.path(
                    dgad.label_encoders, "encoder_81_classes.npy"
                ) as labels_path:
                    self.model_multi = Model(
                        filepath=model_path,
                        labels=load_labels(labels_path),
                        custom_objects={"TCN": TCN},
                    )

    def prepare_domains(
        self, raw_domains: Set[str], max_length: int = 0
    ) -> Tuple[List[Domain], List[str]]:
        """
        preprocesses the domains, tokenizing and applying padding to have same size as binary model
        """
        # TODO: padding may be different for the multi model...! That could lead to issues. Same size should be documented or enforced...
        if not max_length:
            max_length = self.model_binary.data.input_shape[1]
        raw_domains_todo, domains_to_skip = separate_domains_that_are_too_long(
            raw_domains, max_length
        )
        domains_todo = [
            Domain(raw=raw_domain, padded_length=max_length)
            for raw_domain in raw_domains_todo
        ]
        if domains_to_skip:
            logging.warning(
                f"will skip domains {domains_to_skip} because they are too long for the binary model"
            )
        return domains_todo, domains_to_skip

    def investigate_binary(self, word: Word) -> None:
        x_test = np.array([word.padded_token_vector])
        y_test = self.model_binary.data.predict(x_test, verbose=0)
        word.binary_score = float(y_test[0][0])
        word.binary_label = self.model_binary.labels[int(np.round(word.binary_score))]

    def batch_investigate_binary(self, words: List[Word]) -> None:
        batch_padded_token_vector = list()
        for word in words:
            batch_padded_token_vector.append(word.padded_token_vector)
        if len(batch_padded_token_vector) == 0:
            return

        x_test = np.array(batch_padded_token_vector)
        y_test = self.model_binary.data(x_test)

        for index, word in enumerate(words):
            word.binary_score = float(y_test[index][0])
            word.binary_label = self.model_binary.labels[
                int(np.round(word.binary_score))
            ]

    def investigate_family(self, word: Word) -> None:
        x_test = np.array([word.padded_token_vector])
        y_test = self.model_multi.data.predict(x_test, verbose=0)
        best_class_label_index = np.argmax(y_test, axis=1)[0]
        word.family_score = float(np.max(y_test, axis=1)[0])
        word.family_label = self.model_multi.labels[best_class_label_index]

    def batch_investigate_family(self, words: List[Word]) -> None:
        batch_padded_token_vector = list()
        for word in words:
            if word.binary_score <= self.statistical_significance:
                # skip non-dga words
                continue
            else:
                batch_padded_token_vector.append(word.padded_token_vector)
        if len(batch_padded_token_vector) == 0:
            return

        x_test = np.array(batch_padded_token_vector)
        y_test = self.model_multi.data(x_test)

        index_of_predictions = 0
        for word in words:
            if word.binary_score <= self.statistical_significance:
                continue
            else:
                best_class_label_index = np.argmax(y_test[index_of_predictions])
                word.family_score = float(np.max(y_test[index_of_predictions]))
                word.family_label = self.model_multi.labels[int(best_class_label_index)]
                index_of_predictions += 1

    def investigate(self, domains: List[Domain]) -> None:
        """
        performs binary and family predictions on provided list of domains.
        predictions are stored in the words attributes
        """
        # TODO: test performance
        batch_input = list()
        for domain in domains:
            for word in domain.words:
                batch_input.append(word)
        self.batch_investigate_binary(batch_input)
        self.batch_investigate_family(batch_input)

        for domain in domains:
            is_dga = False
            for word in domain.words:
                if word.binary_score > self.statistical_significance:
                    is_dga = True
            if is_dga:
                domain.is_dga = True
                domain.set_family()
            log_prediction(domain)


def log_prediction(domain: Domain) -> None:
    """
    reports to stdout outcome of classification
    """
    logging.info(
        f"{domain.raw}, is_dga: {domain.is_dga}, family: {domain.family_label}"
    )
    for word in domain.words:
        logging.debug(asdict(word))


def pretty_print(domains: List[Domain], output_format: str = "json") -> None:
    dicts = [asdict(domain) for domain in domains]
    for domain in dicts:
        for word in domain["words"]:
            del word["padded_token_vector"]
    if output_format == "json":
        print(json.dumps(dicts, indent=2))
